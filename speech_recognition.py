import requests
import pyttsx3
from test_getAlarmInfo import getAlarmList

class SpeechRecognizer:
    def __init__(self):
        # 初始化语音识别相关参数等
        self.engine = pyttsx3.init()
        
        self.url = 'http://localhost:9000/asr'
        
        self.params = {
            'encode': 'true',
            'task': 'transcribe',
            'language': 'zh',
            'word_timestamps': 'false',
            'output': 'txt'
        }

        self.headers = {
            'accept': 'application/json'
        }
        
        self.waked = False

    def recognize_speech(self, audio_file):
        # 发送HTTP请求进行语音识别
        files = {
            'audio_file': open("./wavs/denoise_" + audio_file, 'rb')
        }
        response = requests.post(self.url, params=self.params, files=files, headers=self.headers)
        words = response.text.strip()
        return words

    def process_response(self, words):
        # 处理语音识别结果
        if words == "你好":
            self.engine.say("你好，有什么需要帮助的？")
            self.engine.runAndWait()
        elif words in ["查询报警", "查詢报警", "查詢報警"]:
            lines = getAlarmList()
            for line in lines:
                self.engine.say(line)
                self.engine.runAndWait()
        # else:
        #     self.engine.say("晴天")
        #     self.engine.runAndWait()

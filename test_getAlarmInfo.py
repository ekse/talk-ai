import xml.etree.ElementTree as ET

# 定义 XML 字符串
xml_str = '''
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
    <GetAllActiveEventsResponse xmlns="http://tempuri.org/">
      <GetAllActiveEventsResult xmlns:a="http://schemas.datacontract.org/2004/07/MGrid.Monitor.OracleEF.ActiveData" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
        <a:ActiveEvent xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" z:Id="i1">
          <a:ConditionID>1</a:ConditionID>
          <a:ConfirmTime i:nil="true"/>
          <a:ConfirmerName/>
          <a:EndTime i:nil="true"/>
          <a:EquipID>42</a:EquipID>
          <a:EquipName>房间空调</a:EquipName>
          <a:EventID>10001</a:EventID>
          <a:EventLevel>3</a:EventLevel>
          <a:EventName>设备通讯状态</a:EventName>
          <a:EventValue>0</a:EventValue>
          <a:LastUpdateTime>2024-04-11T18:27:15</a:LastUpdateTime>
          <a:Meaning>通讯中断</a:Meaning>
          <a:RebootFlag>0</a:RebootFlag>
          <a:RoomID>19</a:RoomID>
          <a:RoomName>IDM</a:RoomName>
          <a:StartTime>2024-04-11T18:27:15</a:StartTime>
        </a:ActiveEvent>
        <a:ActiveEvent xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" z:Id="i2">
          <a:ConditionID>1</a:ConditionID>
          <a:ConfirmTime i:nil="true"/>
          <a:ConfirmerName/>
          <a:EndTime i:nil="true"/>
          <a:EquipID>30</a:EquipID>
          <a:EquipName>空调2</a:EquipName>
          <a:EventID>10001</a:EventID>
          <a:EventLevel>3</a:EventLevel>
          <a:EventName>设备通讯状态</a:EventName>
          <a:EventValue>0</a:EventValue>
          <a:LastUpdateTime>2024-04-13T21:20:33</a:LastUpdateTime>
          <a:Meaning>通讯中断</a:Meaning>
          <a:RebootFlag>0</a:RebootFlag>
          <a:RoomID>19</a:RoomID>
          <a:RoomName>IDM</a:RoomName>
          <a:StartTime>2024-04-13T21:20:33</a:StartTime>
        </a:ActiveEvent>
        <a:ActiveEvent xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" z:Id="i3">
          <a:ConditionID>1</a:ConditionID>
          <a:ConfirmTime i:nil="true"/>
          <a:ConfirmerName/>
          <a:EndTime i:nil="true"/>
          <a:EquipID>40</a:EquipID>
          <a:EquipName>温湿度7</a:EquipName>
          <a:EventID>10001</a:EventID>
          <a:EventLevel>2</a:EventLevel>
          <a:EventName>设备通讯状态</a:EventName>
          <a:EventValue>0</a:EventValue>
          <a:LastUpdateTime>2024-04-11T18:28:56</a:LastUpdateTime>
          <a:Meaning>通讯中断</a:Meaning>
          <a:RebootFlag>0</a:RebootFlag>
          <a:RoomID>19</a:RoomID>
          <a:RoomName>IDM</a:RoomName>
          <a:StartTime>2024-04-11T18:28:56</a:StartTime>
        </a:ActiveEvent>
        <a:ActiveEvent xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" z:Id="i4">
          <a:ConditionID>1</a:ConditionID>
          <a:ConfirmTime i:nil="true"/>
          <a:ConfirmerName/>
          <a:EndTime i:nil="true"/>
          <a:EquipID>27</a:EquipID>
          <a:EquipName>空调1</a:EquipName>
          <a:EventID>10001</a:EventID>
          <a:EventLevel>3</a:EventLevel>
          <a:EventName>设备通讯状态</a:EventName>
          <a:EventValue>0</a:EventValue>
          <a:LastUpdateTime>2024-04-13T22:18:17</a:LastUpdateTime>
          <a:Meaning>通讯中断</a:Meaning>
          <a:RebootFlag>0</a:RebootFlag>
          <a:RoomID>19</a:RoomID>
          <a:RoomName>IDM</a:RoomName>
          <a:StartTime>2024-04-13T22:18:17</a:StartTime>
        </a:ActiveEvent>
        <a:ActiveEvent xmlns:z="http://schemas.microsoft.com/2003/10/Serialization/" z:Id="i5">
          <a:ConditionID>1</a:ConditionID>
          <a:ConfirmTime i:nil="true"/>
          <a:ConfirmerName/>
          <a:EndTime i:nil="true"/>
          <a:EquipID>41</a:EquipID>
          <a:EquipName>温湿度8</a:EquipName>
          <a:EventID>10001</a:EventID>
          <a:EventLevel>2</a:EventLevel>
          <a:EventName>设备通讯状态</a:EventName>
          <a:EventValue>0</a:EventValue>
          <a:LastUpdateTime>2024-04-11T18:28:57</a:LastUpdateTime>
          <a:Meaning>通讯中断</a:Meaning>
          <a:RebootFlag>0</a:RebootFlag>
          <a:RoomID>19</a:RoomID>
          <a:RoomName>IDM</a:RoomName>
          <a:StartTime>2024-04-11T18:28:57</a:StartTime>
        </a:ActiveEvent>
      </GetAllActiveEventsResult>
    </GetAllActiveEventsResponse>
  </s:Body>
</s:Envelope>
'''

# 解析 XML 字符串
root = ET.fromstring(xml_str)

# 定义命名空间
namespace = {
    's': 'http://schemas.xmlsoap.org/soap/envelope/',
    'a': 'http://schemas.datacontract.org/2004/07/MGrid.Monitor.OracleEF.ActiveData'
}

# 查找 EquipName、Meaning 和 LastUpdateTime
def getAlarmList():
    alarmList = []
    for active_event in root.findall('.//a:ActiveEvent', namespace):
        equip_name = active_event.find('./a:EquipName', namespace).text
        meaning = active_event.find('./a:Meaning', namespace).text
        last_update_time = active_event.find('./a:LastUpdateTime', namespace).text
        errorNotice = equip_name + "在" + last_update_time + meaning
        # print("EquipName:", equip_name)
        # print("Meaning:", meaning)
        # print("LastUpdateTime:", last_update_time)
        # print()
        alarmList.append(errorNotice)\

    return alarmList
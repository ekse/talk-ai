import sqlite3

class DB:
    def __init__(self):
        # 连接到数据库（如果不存在则会创建一个新的数据库）
        self.conn = sqlite3.connect('sqlite.db')
        # 创建一个游标对象，用于执行 SQL 语句
        self.cursor = self.conn.cursor()

    async def set_audio_name(self, name):
        # 更新数据
        self.cursor.execute("UPDATE configs SET audio_name = '" + name + "'")
        # 提交事务
        self.conn.commit()

    async def get_audio_name(self):
        # 查询数据
        self.cursor.execute("SELECT * FROM configs")
        rows = self.cursor.fetchall()
        for row in rows:
            print(row)
            return row[0]

    async def set_flag(self, flag):
        # 更新数据
        self.cursor.execute("UPDATE configs SET work_mode = " + str(flag))
        # 提交事务
        self.conn.commit()

    async def get_flag(self):
        # 查询数据
        self.cursor.execute("SELECT * FROM configs")
        rows = self.cursor.fetchall()
        for row in rows:
            print(row)
            return row[1]

import subprocess

def apply_noise_reduction(input_file, output_file):
    # Specify ffmpeg command with noise reduction filter and settings
    ffmpeg_command = [
        'ffmpeg', '-i', input_file,
        '-af', 'afftdn=nt=0.01',  # Corrected noise reduction filter
        output_file
    ]

    # Execute ffmpeg command
    subprocess.run(ffmpeg_command)

if __name__ == "__main__":
    input_file = "./wavs/output_1713369965.wav"
    output_file = "./output_audio.wav"
    apply_noise_reduction(input_file, output_file)

import asyncio

from speech_recognition import SpeechRecognizer
from record import Recorder

async def main():
    recorder = Recorder()
    speech_recognizer = SpeechRecognizer()
    
    while True:
        # 异步获取文件名
        audio_file = await recorder.record_audio()
        await recorder.save_audio(audio_file)
        await recorder.denoise_audio(audio_file)
        
        transcription = speech_recognizer.recognize_speech(audio_file)
        print(transcription)
        # 删除文件
        await recorder.delete_audio(audio_file)
        # 等待响应
        response = speech_recognizer.process_response(transcription)
        await asyncio.sleep(2)

# 运行主程序
if __name__ == "__main__":
    asyncio.run(main())

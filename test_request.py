import requests

# 定义请求的 WSDL URL
wsdl_url = "http://192.168.1.16:9999/ActiveDataService/"

# 定义 SOAPAction
soap_action = "http://tempuri.org/IActiveDataService/GetAllActiveEvents"

# 定义 XML 消息体
xml_body = '''
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
    <s:Body>
    </s:Body>
</s:Envelope>
'''

try:
    # 构建请求头
    headers = {
        "Content-Type": "text/xml; charset=utf-8",
        "soapaction": soap_action,
        'accept-encoding': 'gzip, deflate',
		'connection': 'Keep-Alive',
		'expect': '100-continue'
    }

    # 发送 POST 请求
    response = requests.post(wsdl_url, data=xml_body, headers=headers)

    # 检查响应状态码
    if response.status_code == 200:
        # 打印响应内容
        print(response.text)
    else:
        print("Failed to retrieve WSDL file. Status code:", response.status_code)

except Exception as e:
    print("An error occurred:", e)
